from django.db import models
from datetime import datetime

# Create your models here.

class Node(models.Model):
    mac_address = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    creation_date = models.DateTimeField(default=datetime.now)
    def __str__(self):
        return self.name + " / " + self.mac_address
    class Meta:
        db_table = "node"

class Data(models.Model):
    node = models.ForeignKey(Node, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(default=datetime.now)
    value = models.IntegerField()
    def __str__(self):
        return self.node.name + " / " + self.node.mac_address + ": " + str(self.value)
    class Meta:
       db_table = "data"
