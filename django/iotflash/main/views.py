from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.urls import reverse
from django.views import generic
from .models import *

def error_404(request, exception):
    return render(request, 'main/error_404.html', {}, status=404)

class IndexView(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'nodes'
    def get_queryset(self):
        return Node.objects.order_by('id')

class StatisticsView(generic.ListView):
    template_name = 'main/statistics.html'
    context_object_name = 'data'
    def get_queryset(self):
        return Data.objects.all()

def details_redirect(request, pk):
    return HttpResponseRedirect(reverse('main:details', args={pk}))

class DetailsView(generic.DetailView):
    model = Node
    template_name = 'main/details.html'

class ManagementView(generic.ListView):
    template_name = 'main/management.html'
    context_object_name = 'nodes'
    def get_queryset(self):
        return Node.objects.order_by('id')
    
def changename(request, node_id):
    try:
        node = Node.objects.get(pk=node_id)
        newName = request.POST['nodename']
        if(not newName or newName.isspace()):
            return HttpResponseRedirect(reverse('main:details', args={node_id}))
        node.name = newName
        node.save()
        return HttpResponseRedirect(reverse('main:changeresult'))
    except Node.DoesNotExist:
        return render(request, 'main/error_404.html', {}, status=404)

class ChangeResultView(generic.TemplateView):
        template_name = 'main/changeresult.html'
