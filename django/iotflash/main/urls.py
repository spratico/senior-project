from django.urls import path

from . import views

app_name = 'main'
urlpatterns = [
            path('', views.IndexView.as_view(), name='index'),
            path('statistics/', views.StatisticsView.as_view(), name="statistics"),
            path('<int:pk>/', views.details_redirect),
            path('<int:pk>/details/', views.DetailsView.as_view(), name="details"),
            path('management/', views.ManagementView.as_view(), name="management"),
            path('changeresult/', views.ChangeResultView.as_view(), name="changeresult"),
            path('<int:node_id>/changename/', views.changename, name="changename"),
            ]
