## Sam Pratico ##
## Senior Project 2018-2019 ##
## Advisor: Dr. Plante ##

### Current State

* Learning everything about Bryan's project and getting RBs and router working. 

## Bryan Pearson ##
## Senior Research 2017-2018 ##
## Advisor: Dr. Plante ##

### Current Known Issues/Bugs

* Since the manage script must rest serverside, we need to implement an API that communicates between local users and the manage script on the server. Obviously we need to implement authentication for this. Since Django seems to offer Oauth, web apps seems ideal for this scenario.

* Currently, for best performance, it is expected that each master node searches for unique data in the cluster (that is, if two masters exist, and master A is configured to search for data X, then master B should not also search for data X). This is due to the parallelization of the data pull invoked by 'wd_manager'. If, by contrast, two masters were searching for similar data, it is possible that both could pull from the same node simultaneously, resulting in duplicate data. A potential solution is to create a temporary flag/file on devices which are currently the recipient of a data pull request.

