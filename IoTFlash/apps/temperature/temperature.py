import random, time

temperature = 70
maxTemp = 110
minTemp = 30

while True:
    time.sleep(30)
    dev = random.randint(0, 5)
    maxDiff = maxTemp - temperature
    minDiff = temperature - minTemp

    direct = random.randint(0, maxDiff + minDiff)
    
    if direct < maxDiff :
        temperature = temperature + dev
        if temperature > maxTemp:
            temperature = maxTemp
    else:
        temperature = temperature - dev
        if temperature < minTemp:
            temperature = minTemp

    print(temperature)
    with open("data", "a") as o:
        o.write(str(temperature) + "\n")
