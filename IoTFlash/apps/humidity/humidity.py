import random, time

humidity = 50
maxHum = 110
minHum = 30

while True:
    time.sleep(30)
    dev = random.randint(0, 3)
    maxDiff = maxHum - humidity
    minDiff = humidity - minHum

    direct = random.randint(0, maxDiff + minDiff)
    
    if direct < maxDiff :
        humidity = humidity + dev
        if humidity > maxHum:
            humidity = maxHum
    else:
        humidity = humidity - dev
        if humidity < minHum:
            humidity = minHum

    print(humidity)
    o = open("data", "a")
    o.write(str(humidity) + "\n")
    o.close()    
