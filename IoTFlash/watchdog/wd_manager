#!/usr/bin/env python

'''
The watchdog manager runs as a background process on the server. Every x minutes/hours/days (discretion of the admin),
the manager contacts the master nodes and runs the wd_exec scripts. Afterwords, the masters now contain the data and
the manager can pull this data into the server and push it to the server.
'''

from fabric.api import *
from sys import argv
import psycopg2, re
from glob import glob
import os

# Expected arguments:
# 1: The configuration file

srcDir = argv[1]
configFile = argv[2]
serverKey = ""
masterKey = ""
watchDir = ""
watchData = ""
watchConfig = ""
watchExec = ""
database = {}
devices = {}
masters = []

# Get keys, watchdog directory, data file, and config file
f = open("%s/%s" % (srcDir, configFile), "r")
while True:
    line = f.readline()
    if not line:
        break
    if re.match(r'^\s*$', line):
        continue

    lineArgs = line.split()

    if lineArgs[0] == "SECRET-CONFIG":
        secretConfigFile = lineArgs[1]

    if lineArgs[0] == "WATCH-DIR":
        watchDir = lineArgs[1]

    if lineArgs[0] == "WATCH-DATA":
        watchData = lineArgs[1]

    if lineArgs[0] == "WATCH-CONFIG":
        watchConfig = lineArgs[1]

    if lineArgs[0] == "WATCH-LOCAL-EXEC":
        watchExec = lineArgs[1]

f.close()
f = open("%s/%s" % (srcDir, secretConfigFile), "r")
while True:
    line = f.readline()
    if not line:
        break
    if re.match(r'^\s*$', line):
        continue

    lineArgs = line.split()

    if lineArgs[0] == "SERVER-KEY":
        serverKey = lineArgs[1]

    if lineArgs[0] == "MASTER-KEY":
        masterKey = lineArgs[1]

    if lineArgs[0] == "DB-TYPE":
        database['dbtype'] = lineArgs[1]

    if lineArgs[0] == "DB-NAME":
        database['dbname'] = lineArgs[1]

    if lineArgs[0] == "DB-USER":
        database['dbuser'] = lineArgs[1]

    if lineArgs[0] == "DB-PASSWORD":
        database['dbpass'] = lineArgs[1]

    if lineArgs[0] == "DB-HOST":
        database['dbhost'] = lineArgs[1]

# Log in to database
database['conn'] = "dbname=%s user=%s host=%s password=%s" % (database['dbname'], database['dbuser'], database['dbhost'], database['dbpass'])
try:
    conn = psycopg2.connect(database['conn'])
except:
    print("Error: Cannot connect to database. Check your configuration.")
    exit(1)

cur = conn.cursor()

# Select the usernames and hostnames of all devices, add to devices dict
cur.execute("""SELECT hostname,username FROM nodes""")

for x in cur.fetchall():
    hostname = ''.join(x[0]).replace(' ', '')
    username = ''.join(x[1]).replace(' ', '')
    devices[hostname] = username

# Select the hostnames of all MASTERS, add to masters list
cur.execute("""SELECT hostname FROM nodes 
            JOIN masters ON 
            nodes.id = masters.node_id""")

for x in cur.fetchall():
    masterHostname = ''.join(x[0]).replace(' ', '')
    masters.append(masterHostname)

# Clean devices dict for exec script
devicesClean = "'{"
for d in devices.keys():
    devicesClean = devicesClean + '"' + d + '": "' + devices[d] + '", '

devicesClean = devicesClean[0:len(devicesClean) - 2] + "}'"

# SSH into device and call wd_exec
for master in masters:
    with settings(host_string = '%s.local' % master, user = devices[master], key_filename = serverKey):
        run('python %s/%s %s %s %s %s %s' % (watchDir, watchExec, devicesClean, watchDir, masterKey, watchData, watchConfig))

        # Check for exec-flag until gone
        while(True):
            execFlag = run('ls %s | grep exec-flag' % watchDir, quiet=True)
            if execFlag == 'exec-flag':
                continue
            else:
                break
    
        # Append master data-output to local data-output
        local("scp -r %s@%s.local:%s/data-output %s" % (devices[master], master, watchDir, srcDir));

        # Delete master output-data
        run('sudo rm -rv %s/data-output' % watchDir)

# Insert data into database
cur.execute(
        "PREPARE insertPlan AS "
        "INSERT INTO data (node_id, name, value) "
        "VALUES ($1, $2, $3)")
for root, directories, filenames in os.walk("%s/data-output" % srcDir):
    for filename in filenames:
        nodePath = os.path.join(root, filename)
        nodePathR = nodePath[::-1]
        nodeIndex = nodePathR.index('/')
        node = nodePath[-(nodeIndex):]

        dataPath = os.path.dirname(nodePath)
        dataPathR = dataPath[::-1]
        dataIndex = dataPathR.index('/')
        data = dataPath[-(dataIndex):]
        
        cur.execute('SELECT id,hostname FROM nodes')

        results = cur.fetchall()

        for r in results:
            if r[1].strip(' ') == node:
                f = open(nodePath, "r")
                while True:
                    line = f.readline()
                    if not line:
                        break
                    if re.match(r'^\s*$', line):
                        continue

                    cur.execute("execute insertPLAN (%s, %s, %s)", (r[0], data, line))
                    conn.commit()

# Remove data-output
local('rm -rv %s/data-output' % srcDir) 
