from django.shortcuts import render


def homepage(request):
    return render(request, 'homepage/homepage.html')


# def loginPage(request):
#     return render(request, 'accounts/login')
