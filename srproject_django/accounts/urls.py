# The URLs provided by auth are:

# accounts/login/ [name='login']
# accounts/logout/ [name='logout']
# accounts/password_change/ [name='password_change']
# accounts/password_change/done/ [name='password_change_done']
# accounts/password_reset/ [name='password_reset']
# accounts/password_reset/done/ [name='password_reset_done']
# accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
# accounts/reset/done/ [name='password_reset_complete']

from django.conf.urls import url
from . import views

app_name = 'accounts'

urlpatterns = [
    url('signup', views.signup_view, name='signup'),
    url('login', views.login_view, name='login'),
    url('forgot-password', views.forgot_password_view, name='forgot-password'),
    url('logout', views.logout_view, name='logout'),
    # url('homepage_return', views.homepage_return, name='homepage_return'),
]