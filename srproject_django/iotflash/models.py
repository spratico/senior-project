from __future__ import unicode_literals
from djongo import models


# Each model is a class. Each class represents ONE table in a database
# Each type of data (Account, Users) is represented by it's own model
# Each model maps to a single table


class HumidityDevice(models.Model):  # Collection name
    # auto update when data is inserted
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    # auto update when data is altered
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    # Documents
    device_id = models.CharField(max_length=20, null=True)
    device_name = models.CharField(max_length=20, null=True)
    device_location = models.CharField(max_length=100, null=True)
    sensor_type = models.CharField(max_length=20, null=True)
    subbed_topic = models.CharField(max_length=50, null=True)
    humidity_percentage = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.device_name

    class Meta:
        db_table = 'HumidityDevices'
        app_label = 'iotflash'
        # abstract = True


class TempDevice(models.Model):  # Collection name
    # auto update when data is inserted
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    # auto update when data is altered
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    # Documents
    device_id = models.CharField(max_length=20, null=True)
    device_name = models.CharField(max_length=20, null=True)
    device_location = models.CharField(max_length=100, null=True)
    sensor_type = models.CharField(max_length=20, null=True)
    subbed_topic = models.CharField(max_length=50, null=True)
    temp_level = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.device_name

    class Meta:
        db_table = 'TempDevices'
        app_label = 'iotflash'
        # abstract = True

    objects = models.DjongoManager()

# ------------------------------------------------------------

#######################
#                     #
#  IotFlash Devices   #
#  Stored in MongoDB  #
#    Using Djongo     #
#                     #
#######################

# Humdity collection
# class HumidityDevices(models.Model):
#     collectionName = models.CharField(max_length=100)
#
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         # db_table = 'iotflash_db'
#         db_table = 'iotflashTest_db'
#         app_label = 'iotflash'


# Temperature collection
# class TempDevices(models.Model):
#     collectionName = models.CharField(max_length=100)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         # db_table = 'iotflash_db'
#         db_table = 'iotflashTest_db'
#         app_label = 'iotflash'


# ------------------------------------------------------------

# class Node(models.Model):
#     name = models.CharField(max_length=100)
#     creation_date = models.DateTimeField()
#     topic = models.CharField(max_length=100)
#     sensor = models.CharField(max_length=100)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         db_table = 'iotflash_db'
#         app_label = 'iotflash'
#
#
#
# class Data(models.Model):
#     node = models.ForeignKey(Node, on_delete=models.CASCADE)
#     creation_date = models.DateTimeField()
#     value = models.IntegerField(default=0, null=True)
#
#     # topic = models.CharField(default='', max_length=100)
#     # sensor = models.CharField(default='', max_length=100)
#
#     def __str__(self):
#         return self.node
#
#     class Meta:
#         db_table = 'iotflash_db'
#         app_label = 'iotflash'


# class User(models.Model):
#     user_id = models.IntegerField()
#     user_name = models.CharField(max_length=100)
#     user_pass = models.CharField(max_length=100)
#     email = models.CharField(max_length=100)
#
#     def __str__(self):
#         return self.user_name + " / " + self.user_id
#
#     class Meta:
#         db_table = 'users'
#         app_label = 'iotflash'
#     # class Meta:
#     #     db_table = 'auth'
#     #     app_label = 'auth_user'
