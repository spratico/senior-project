from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(HumidityDevice)
admin.site.register(TempDevice)
