# Generated by Django 2.1.1 on 2019-04-09 19:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iotflash', '0011_auto_20190327_1200'),
    ]

    operations = [
        migrations.CreateModel(
            name='HumidityDevices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('collectionName', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'iotflashTest_db',
            },
        ),
        migrations.CreateModel(
            name='TempDevices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('collectionName', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'iotflashTest_db',
            },
        ),
        migrations.RemoveField(
            model_name='data',
            name='node',
        ),
        migrations.DeleteModel(
            name='User',
        ),
        migrations.DeleteModel(
            name='Data',
        ),
        migrations.DeleteModel(
            name='Node',
        ),
    ]
