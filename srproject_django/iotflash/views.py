from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import *


@login_required(login_url='/accounts/login)')
def iotflash_dashboard(request):
    humiditydevices = HumidityDevice.objects.all()
    tempdevices = TempDevice.objects.all()

    return render(request, 'iotflash/dashboard.html')
    # will be able to use when database working
    # return render(request, 'iotflash/dashboard.html', {'humdevs': humiditydevices, 'tempdevs': tempdevices})

def iotflash_profile_settings(request):

    return render(request, 'iotflash/profile_settings.html')


def iotflash_testpage(request):
    humiditydevices = HumidityDevice.objects.all()
    tempdevices = TempDevice.objects.all()

    return render(request, 'iotflash/blank.html', {'humdevs': humiditydevices, 'tempdevs': tempdevices})


def device_table_view(request):
    humiditydevices = HumidityDevice.objects.all()
    tempdevices = TempDevice.objects.all()
    return render(request, 'iotflash/devices_table.html', {'humdevs': humiditydevices, 'tempdevs': tempdevices})


def app_view(request):
    return render(request, 'iotflash/applications.html')


def device_chart_view(request):
    return render(request, 'iotflash/devices_chart.html')


# def iotflash_device_detail(request, devID):  # blank will be replaced with USER_ID from database
def iotflash_device_detail(request):  # blank will be replaced with USER_ID from database

    # humiditydevice = HumidityDevice.objects.get(devID=devID)
    # tempdevice = TempDevice.objects.get(devID=devID)
    # return render(request, 'iotflash/device_detail.html', {'humdevice': humiditydevice, 'tempdevice': tempdevice})
    return render(request, 'iotflash/device_info.html')


# def device_table_view(request):
#     return render(request, '/iotflash/devices_table.html')

# ---------------------------------------------------------------

# MOST RECENT USE THIS IS ANYTHING

# def error_404(request, exception):
#     return render(request, 'iotflash/404.html', {}, status=404)
#
# # class IndexView(generic.ListView):
# #     template_name = 'iotflash/details.html'
# #     context_object_name = 'nodes'
# #     def get_queryset(self):
# #         return Node.objects.order_by('id')
#
# def TESTINGPAGE(request):
#     return render_to_response('iotflash/blank.html') #blank will be replaced with USER_ID from database
#
# def homepage(request):
#     return render_to_response('iotflash/homepage.html')
#
# def homepageRedirect(request):
#     response = redirect('iotflash/homepage')
#     return response
#
# def base(request):
#     return render_to_response('iotflash/base.html')
#
# def loginHomeRedirect(request):
#     response = redirect('../..')
#     return response

# ------------------------------------------------------------------------------------

# def index(request):
#     # node_data = Node.objects.all()
#     # for field in node_data:
#     #     print('Device name: ', field.name)
#     #     print('Device ID: ', field.id)
#     #     print('Sensor type: ', field.sensor)
#     #     print('Topic: ', field.topic)
#     #
#     #     data_data = Data.objects.all()
#     # for field in data_data:
#     #         print('Device ID: ', field.id)
#     #         print('Device data value: ', field.value)
#
#     return render_to_response('iotflash/index.html')

# def dash(request):
#     return render_to_response('iotflash/index.html')

# def home(request):
#     return render_to_response('iotflash/home.html')

# def loginPage(request):
#     return render_to_response('iotflash/accounts_template.html')

# def registerPage(request):
#     return render_to_response('iotflash/register.html')
#
# def error404Page(request):
#     return render_to_response('iotflash/404.html')
#
# def forgotPassPage(request):
#     return render_to_response('iotflash/forgot-password.html')
#
# def uaPage(request):
#     return render_to_response('iotflash/utilities-animation.html')


# def index(request):
#     node_data = Node.objects.all()
#     for field in node_data:
#         print('Device name: ', field.name)
#         print('Device ID: ', field.id)
#         print('Sensor type: ', field.sensor)
#         print('Topic: ', field.topic)
#
#     data_data = Data.objects.all()
#     for field in data_data:
#         print('Device ID: ', field.id)
#         print('Device data value: ', field.value)
#
#     return HttpResponse("Items being printed in terminal")


# def buttons(request):
#     return render_to_response('iotflash/buttons.html')
#
# def cards(request):
#     return render_to_response('iotflash/cards.html')
#
# def charts(request):
#     return render_to_response('iotflash/charts.html')
#
# def tables(request):
#     return render_to_response('iotflash/devices_table.html')
#
# def ua(request):
#     return render_to_response('iotflash/utilities-animation.html')
#
# def ub(request):
#     return render_to_response('iotflash/utilities-border.html')
#
# def uc(request):
#     return render_to_response('iotflash/utilities-color.html')
#
# def uo(request):
#     return render_to_response('iotflash/utilities-other.html')
