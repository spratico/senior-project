from django.apps import AppConfig


class IotflashConfig(AppConfig):
    name = 'iotflash'
