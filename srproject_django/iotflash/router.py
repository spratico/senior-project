class IotflashRouter(object):
    """A router to control all database operations on models in
    the iotflash application"""


def db_for_read(self, model, **hints):
    if model._meta.app_label == 'iotflash':
        return 'iotflashDevices'
    return None


def db_for_write(self, model, **hints):
    if model._meta.app_label == 'iotflash':
        return 'iotflashDevices'
    return None


def allow_relation(self, obj1, obj2, **hints):
    if obj1._meta.app_label == 'iotflash' or obj2._meta.app_label == 'iotflash':
        return True
    return None


def allow_syncdb(self, db, model):
    if db == 'iotflashDevices':
        return model._meta.app_label == 'iotflash'
    elif model._meta.app_label == 'iotflash':
        return False
    return None
