# from django.urls import path
# from django.urls import path, include
# from django.views.generic.base import TemplateView
# from . import views

from django.conf.urls import url
from . import views

app_name = 'devices'

urlpatterns = [

    url('dashboard/', views.iotflash_dashboard, name='dashboard'),
    url('test-page/', views.iotflash_testpage, name='testpage'),
    url('applications/', views.app_view, name='applications'),
    url('device-table/', views.device_table_view, name='device-table'),
    url('device-chart/', views.device_chart_view, name='device-chart'),
    # url('^(?P<devID>[\w-]+)/$', views.iotflash_device_detail, name='details'),
    url('device-info', views.iotflash_device_detail, name='details'),
    url('profile-settings', views.iotflash_profile_settings, name='profile-settings'),

    # path('', views.base, name='base'),
    # path('dash', views.dash, name='dash'),
    # path('homepage', views.homepage),
    # path('blank', views.TESTINGPAGE),
    # path('login/homepageRedirect', views.homepageRedirect),

    # path('base', views.base),
    # path('', views.index, name='index'),
    # path('login', views.loginPage, name='login'),
    # path('register', views.registerPage, name='register'),
    # path('error404', views.error404Page, name='error404'),
    # path('forgot-password', views.forgotPassPage, name='forgot-password'),

    # path('buttons.html', views.buttons),
    # path('cards.html', views.cards),
    # path('charts.html', views.charts),
    # path('devices_table.html', views.tables),
    # path('ua', views.uaPage, name='ua'),
    # path('utilities-border.html', views.ub),
    # path('utilities-color.html', views.uc),
    # path('utilities-other.html', views.uo),

    # path('home', views.home)

]
